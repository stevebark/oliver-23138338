import time

def process_bar(job_id):
    time.sleep(10)
    if job_id % 2 != 0:
        raise Exception(f"I don't like odd numbers like {job_id}")
    return {
        'bar': f'This is the result of job_id {job_id}'
    }
